import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { HttpClientModule, HTTP_INTERCEPTORS } from '@angular/common/http';

import {BrowserAnimationsModule} from '@angular/platform-browser/animations';
import {MatToolbarModule} from '@angular/material/toolbar';
import { MatIconModule, MatMenuModule, MatButtonModule, MatSnackBarModule, MatProgressSpinnerModule } from '@angular/material';
import { NotificationMessageComponent, LoaderComponent } from './shared/components';
import { AppHttpInterceptor } from './shared/api/app-http-interceptor';
import { LoaderService } from './shared/services/loader.service';
import { SharedModule } from './shared/shared.module';

@NgModule({
  declarations: [
    AppComponent
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    HttpClientModule,
    BrowserAnimationsModule,
    MatToolbarModule,
    MatIconModule,
    MatMenuModule,
    MatButtonModule,
    MatSnackBarModule,
    MatProgressSpinnerModule,
    SharedModule
  ],
  entryComponents: [
    NotificationMessageComponent
  ],
  providers: [
    { provide: HTTP_INTERCEPTORS, useClass: AppHttpInterceptor, multi: true },
    LoaderService
  ],
  bootstrap: [AppComponent]
})
export class AppModule { }
