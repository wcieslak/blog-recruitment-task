import { Component, OnInit, OnDestroy } from '@angular/core';
import {takeUntil} from 'rxjs/operators';

import { Router, ActivatedRoute, ParamMap } from '@angular/router';
import { PostService } from '../../services';
import { Subject, Observable } from 'rxjs';
import { Post } from '@root/app/shared/models/post.model';
import { NotificationMessageService } from '@root/app/shared/services/notification-message.service';
import { MessageType } from '@root/app/shared/constants/message-type.constant';
import { ChangeDetectionStrategy } from '@angular/compiler/src/core';

@Component({
  selector: 'app-edit-post',
  templateUrl: './edit-post.component.html',
  styleUrls: ['./edit-post.component.scss']
})
export class EditPostComponent implements OnInit, OnDestroy {
  public destroy$: Subject<void> = new Subject<void>();
  public post$: Observable<Post>;
  private currentPostId: string;

  constructor(
    private postService: PostService,
    private router: Router,
    private route: ActivatedRoute,
    private notificationMessageService: NotificationMessageService,
  ) { }

  ngOnInit() {
    this.route.paramMap
    .pipe(takeUntil(this.destroy$))
    .subscribe((params: ParamMap) => {
      this.currentPostId = params.get('postId');
      this.post$ = this.postService.getPost(
        this.currentPostId
      );
    });
  }

  public onFormSubmit(post: Post): void {
    this.postService.updatePost(this.currentPostId, post)
      .pipe(takeUntil(this.destroy$))
      .subscribe(
        (response) => {
          this.router.navigate(['/']);
        },
        error => {
          this.notificationMessageService.openBarMessage(error.message, MessageType.ERROR);
        }
      );
  }

  public ngOnDestroy(): void {
    this.destroy$.complete();
  }

}
