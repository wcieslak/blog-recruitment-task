import { PostsListComponent } from './posts-list';
import { CommentsListComponent } from './comments-list';
import { AddPostComponent } from './add-post';
import { EditPostComponent } from './edit-post';
import { BlogWrapperComponent } from './blog-wrapper';

export const entities: any[] = [
    BlogWrapperComponent,
    PostsListComponent,
    AddPostComponent,
    EditPostComponent,
    CommentsListComponent
];

export * from './blog-wrapper';
export * from './posts-list';
export * from './comments-list';
export * from './add-post';
export * from './edit-post';
