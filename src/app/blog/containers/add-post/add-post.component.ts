import { Component, OnInit, OnDestroy } from '@angular/core';
import {takeUntil} from 'rxjs/operators';

import { PostService } from '../../services';
import { Post } from '@root/app/shared/models/post.model';
import { Subject } from 'rxjs';
import { Router } from '@angular/router';
import * as uuid from 'uuid';
import { NotificationMessageService } from '@root/app/shared/services/notification-message.service';
import { MessageType } from '@root/app/shared/constants/message-type.constant';

@Component({
  selector: 'app-add-post',
  templateUrl: './add-post.component.html',
  styleUrls: ['./add-post.component.scss']
})
export class AddPostComponent implements OnInit, OnDestroy {
  public destroy$: Subject<void> = new Subject<void>();
  Id: string;

  constructor(
    private postService: PostService,
    private router: Router,
    private notificationMessageService: NotificationMessageService,
    ) { }

  ngOnInit() { }

  public onFormSubmit(post: Post): void {
    post.id = uuid.v4();
    this.postService.addPost(post)
      .pipe(takeUntil(this.destroy$))
      .subscribe(
        (response) => {
          this.router.navigate(['/']);
        },
        (error) => {
          this.notificationMessageService.openBarMessage(error.message, MessageType.ERROR);
        }
      );
  }

  public ngOnDestroy(): void {
    this.destroy$.complete();
  }
}
