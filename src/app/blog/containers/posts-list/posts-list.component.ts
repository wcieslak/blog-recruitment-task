import { Component, OnInit, OnDestroy } from '@angular/core';
import { Observable, Subject } from 'rxjs';
import { Post } from '@root/app/shared/models/post.model';
import { PostService } from '../../services/post.service';
import { MatDialog } from '@angular/material';
import { CommentsListComponent } from '../comments-list';
import { takeUntil } from 'rxjs/operators';
import { NotificationMessageService } from '@root/app/shared/services/notification-message.service';
import { MessageType } from '@root/app/shared/constants/message-type.constant';

@Component({
  selector: 'app-posts-list',
  templateUrl: './posts-list.component.html',
  styleUrls: ['./posts-list.component.scss']
})
export class PostsListComponent implements OnInit, OnDestroy {
  public dialogRef: any;
  public posts$: Observable<Post[]>;
  public comments$: Observable<Comment[]>;
  private destroy$: Subject<void> = new Subject();

  constructor(
    private notificationMessageService: NotificationMessageService,
    private postService: PostService,
    public dialog: MatDialog
    ) { }

  ngOnInit() {
    this.posts$ = this.postService.getAllPosts();
  }

  public isPostClicked(id: string) {
    this.comments$ = this.postService.getPostComments(id);
    this.comments$
    .pipe(takeUntil(this.destroy$))
    .subscribe(
      (resp) => {
        if (resp.length) {
          this.dialogRef = this.dialog.open(CommentsListComponent,  {data: resp});
        }
      },
      error => {
        console.log(error);
        this.notificationMessageService.openBarMessage(error.message, MessageType.ERROR);
      }
    );
  }

  public ngOnDestroy(): void {
    this.destroy$.complete();
  }
}
