import { Component, OnInit, Input, EventEmitter, Output, ChangeDetectionStrategy } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { Post } from '@root/app/shared/models/post.model';

@Component({
  selector: 'app-post-form',
  changeDetection: ChangeDetectionStrategy.OnPush,
  templateUrl: './post-form.component.html',
  styleUrls: ['./post-form.component.scss']
})
export class PostFormComponent implements OnInit {
  public postForm: FormGroup;
  @Input() public type: any = 'add';
  @Input() public set post(post: Post) {
    this._post = post;
    if (post) {
      this.rebuildForm();
    }
  }
  @Output() public formSubmit: EventEmitter<Post> = new EventEmitter<Post>();

  public _post: Post;

  constructor(private formBuilder: FormBuilder) { }

  public get isEditForm(): boolean {
    return this.type === 'edit';
  }

  public get submitButtonText(): string {
    return this.isEditForm ? 'Save' : 'Add';
  }

  public get preparedPost(): Post {
    return {
      id: this.postForm.value.id,
      title: this.postForm.value.title,
      author: this.postForm.value.author
    };
  }

  public ngOnInit(): void {
    this.postForm = this.formBuilder.group({
      id: [{value: 0, disabled: this.isEditForm }],
      title: ['', [Validators.required, Validators.maxLength(50)]],
      author: ['', Validators.required]
    });
  }

  private rebuildForm(): void {
    this.postForm.reset({
      id: this._post.id,
      title: this._post.title,
      author: this._post.author
    });
  }

  public onSubmitClick(): void {
    this.formSubmit.emit(this.preparedPost);
  }

}
