import { PostComponent } from './post';
import { CommentComponent } from './comment';
import { PostFormComponent } from './post-form';

export const entities: any[] = [
    PostComponent,
    CommentComponent,
    PostFormComponent
];

export * from './post';
export * from './comment';
export * from './post-form';
