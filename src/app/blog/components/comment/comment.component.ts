import { Component, OnInit, Input, ChangeDetectionStrategy } from '@angular/core';
import { PostComment } from '@root/app/shared/models/comment.model';

@Component({
  selector: 'app-comment',
  changeDetection: ChangeDetectionStrategy.OnPush,
  templateUrl: './comment.component.html',
  styleUrls: ['./comment.component.scss']
})
export class CommentComponent implements OnInit {
  @Input() postComment: PostComment;
  constructor() { }

  ngOnInit() {
  }

}
