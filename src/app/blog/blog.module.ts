import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { RouterModule } from '@angular/router';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';

import { routes } from './blog.routes';
import * as containers from './containers';
import * as components from './components';
import * as services from './services';

import {
  MatCardModule,
  MatInputModule,
  MatFormFieldModule,
  MatIconModule,
  MatButtonModule,
  MatDialogModule,
  MatListModule
} from '@angular/material';
import { CommentsListComponent } from './containers';

@NgModule({
  declarations: [
    ...components.entities,
    ...containers.entities,
    CommentsListComponent
  ],
  imports: [
    CommonModule,
    MatCardModule,
    MatInputModule,
    MatFormFieldModule,
    MatIconModule,
    MatButtonModule,
    MatDialogModule,
    MatListModule,
    FormsModule,
    ReactiveFormsModule,
    RouterModule.forChild(routes),
  ],
  providers: [
    ...services.entities
  ],
  entryComponents: [
    CommentsListComponent
  ]
})
export class BlogModule { }
