import { Routes } from '@angular/router';

import { PostsListComponent } from './containers/posts-list';
import { AddPostComponent, BlogWrapperComponent, EditPostComponent } from './containers';

export const routes: Routes = [
    {
        path: '',
        component: BlogWrapperComponent,
        children: [
            { path: '', component: PostsListComponent },
            { path: 'add', component: AddPostComponent },
            { path: 'edit/:postId', component: EditPostComponent },
        ]
    },
];
