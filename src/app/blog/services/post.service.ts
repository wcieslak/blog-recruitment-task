import { Injectable } from '@angular/core';
import { HttpClient, HttpParams } from '@angular/common/http';
import { Observable } from 'rxjs';

import { Post } from '@root/app/shared/models/post.model';
import { API_URL } from '@root/app/shared/constants/api.constant';
import { map } from 'rxjs/operators';

@Injectable({
  providedIn: 'root'
})
export class PostService {

  constructor(private http: HttpClient) { }

  public getAllPosts(): Observable<Post[]> {
    return this.http.get<Post[]>(`${API_URL}/posts`);
  }
  public getPostComments(postId: string) {
    const params = new HttpParams().set('postId', postId);
    return this.http.get<Comment[]>(`${API_URL}/comments`, {params});
  }

  public addPost(post: Post): Observable<any> {
    return this.http.post(`${API_URL}/posts`, post);
  }

  public getPost(postId: string): Observable<any> {
    return this.http.get(`${API_URL}/posts/${postId}`);
  }

  public updatePost(postId: string, post: Post): Observable<any> {
    return this.http.put(`${API_URL}/posts/${postId}`, post);
  }
}

