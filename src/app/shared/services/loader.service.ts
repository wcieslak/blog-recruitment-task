import { Injectable } from '@angular/core';
import { Subject, ReplaySubject } from 'rxjs';

@Injectable()
export class LoaderService {
  constructor() { }

  public isLoading$ = new Subject<boolean>();

  public showLoader() {
    this.isLoading$.next(true);
  }
  public hideLoader() {
    this.isLoading$.next(false);
  }
}
