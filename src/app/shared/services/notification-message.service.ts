import { Injectable } from '@angular/core';
import { MatSnackBar } from '@angular/material';
import { NotificationMessageComponent } from '../components';

@Injectable({
  providedIn: 'root'
})
export class NotificationMessageService {

  constructor(
    private snackBar: MatSnackBar,
    ) { }

  public openBarMessage(message: string, type?: any): void {
    const durationInSeconds = 10;

    this.snackBar.openFromComponent(NotificationMessageComponent, {
      data: {message, type},
      panelClass: type.class,
      duration: durationInSeconds * 1000,
    });
  }
}
