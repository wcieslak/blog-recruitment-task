export const MessageType = {
  SUCCESS: {
    class: 'success',
     icon: 'check_circle'
  },
  INFO: {
    class: 'info',
     icon: 'info'
  },
  WARN: {
    class: 'warning',
     icon: 'warning'
  },
  ERROR: {
    class: 'error',
     icon: 'error'
  }
};
