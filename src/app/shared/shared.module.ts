import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import * as components from './components';
import { MatProgressSpinnerModule, MatIconModule } from '@angular/material';

@NgModule({
  declarations: [
    ...components.entities
  ],
  imports: [
    CommonModule,
    MatProgressSpinnerModule,
    MatIconModule
  ],
  exports: [
    ...components.entities
  ]
})
export class SharedModule { }
