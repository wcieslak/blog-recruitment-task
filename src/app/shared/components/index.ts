import { NotificationMessageComponent } from './notification-message';
import { LoaderComponent } from './loader';

export const entities: any[] = [
    NotificationMessageComponent,
    LoaderComponent
];

export * from './notification-message';
export * from './loader';
