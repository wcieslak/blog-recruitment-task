import { HttpInterceptor, HttpRequest, HttpHandler, HttpEvent, HttpErrorResponse } from '@angular/common/http';
import { Observable, throwError } from 'rxjs';
import { Injectable } from '@angular/core';
import { map, catchError, finalize } from 'rxjs/operators';
import { MessageType } from '../constants/message-type.constant';
import { NotificationMessageService } from '../services/notification-message.service';
import { Router } from '@angular/router';
import { LoaderService } from '../services/loader.service';

@Injectable()
export class AppHttpInterceptor implements HttpInterceptor {

    constructor(
        private notificationMessageService: NotificationMessageService,
        private loaderService: LoaderService,
        private router: Router,
    ) { }

    intercept(
        request: HttpRequest<any>,
        next: HttpHandler
    ): Observable<HttpEvent<any>> {

        return next.handle(request).pipe(
            map((event: HttpEvent<any>) => {
                this.loaderService.showLoader();
                return event;
            }),
            finalize(() => {
                this.loaderService.hideLoader();
            }
            ),
            catchError((httpError: HttpErrorResponse) => {
                this.notificationMessageService.openBarMessage(httpError.message, MessageType.ERROR);
                return throwError(httpError);
            }));
    }
}
